<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model {

    public $table = 'tbl_branches';

    public function user() {
        return $this->belongsTo('App\Models\User');
    }

}
