<?php

// Homepage Routes
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);

Route::get('/about', ['as' => 'home.about', 'uses' => 'HomeController@about']);

Route::get('/contact', [ 'as' => 'home.contact', 'uses' => 'HomeController@contact']);

Route::get('/branches', ['as' => 'home.branches', 'uses' => 'HomeController@branches']);

Route::get('/services', ['as' => 'home.services', 'uses' => 'HomeController@services']);

Route::get('/promotion', ['as' => 'home.promotion', 'uses' => 'HomeController@promotion']);

Route::group(['namespace' => 'Feeds', 'prefix' => 'feeds'], function(){
    Route::get('json/users', ['as' => 'feeds.json.users', 'uses' => 'JsonController@users']);
});

// Authorization Routes
Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function(){

    Route::get('login', 'AuthController@getLogin');

    Route::post('login', 'AuthController@postLogin');

    Route::get('logout', 'AuthController@getLogout');

    Route::get('register', 'AuthController@getRegister');

    Route::post('register', 'AuthController@postRegister');

});

// Dashboard Routes
Route::group(['namespace' => 'Dash', 'prefix' => 'dash'], function() {

    Route::get('home', 'AdminController@index')->name('dash.home');

    Route::resource('time', 'TimeController');

    Route::resource('place', 'PlaceController');

    Route::resource('user','UserController');

    Route::resource('booking','BookingController');

    Route::get('customer/json', 'CustomerController@json')->name('dash.customer.json');
    Route::resource('customer','CustomerController');

    Route::get('branch/json', 'BranchController@json');

    Route::resource('branch','BranchController');

    Route::get('json', 'JsonController@index')->name('json');

    Route::get('json/users', ['as' => 'json.users', 'uses' => 'JsonController@users']);

});
