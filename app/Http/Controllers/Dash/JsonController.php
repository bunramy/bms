<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use yajra\Datatables\Datatables;

use App\Models\User;

class JsonController extends Controller {

    public function index() {
        return view('json.index');
    }

    public function users() {
        return Datatables::of(User::select('*'))->make(true);
    }

}
