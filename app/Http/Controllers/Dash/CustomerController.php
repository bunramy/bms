<?php

namespace App\Http\Controllers\Dash;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use yajra\Datatables\Datatables;
use App\Models\Customer;

class CustomerController extends Controller {

    public function index() {
        return view('dash.customer.index');
    }

    /**
     * Use to respone json data
     * @return [json]
     */
    public function json() {
        return Datatables::of(Customer::select('*'))->make(true);
    }

    public function create() {
        //
    }

    public function store(Request $request) {
        //
    }

    public function show($id) {
        //
    }

    public function edit($id) {
        //
    }

    public function update(Request $request, $id) {
        //
    }

    public function destroy($id) {
        //
    }
}
