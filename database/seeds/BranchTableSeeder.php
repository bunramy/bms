<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Branch;
class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Branch::class, 10)->create();
        //
        // for($i=0 ; $i<10 ; $i++){
        //     DB::table('tbl_branches')->insert([
        //         'name' => str_random(10),
        //         'address' => str_random(50),
        //         'control_by' => 1,
        //         'phone' => '0' . rand (10000000 , 99999999),
        //         'created_at' => Carbon::now(),
        //         'updated_at' => Carbon::now()
        //     ]);
        // }
    }
}
