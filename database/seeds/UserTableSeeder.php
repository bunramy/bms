<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserTableSeeder extends Seeder {

    public function run() {

        DB::table('tbl_users')->insert([
            'firstname' => 'ok',
            'lastname' => 'Darabopha',
            'username' => 'ok',
            'email' => 'ok@example.com',
            'phone' => '0111111113',
            'address' => 'Phnom Penh',
            'card_no' => '345679123',
            'type' => 'admin',
            'remember_token' => '',
            'password' => bcrypt('password'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('tbl_users')->insert([
            'firstname' => 'Sila',
            'lastname' => 'Soa',
            'username' => 'Sila',
            'email' => 'silasoa@example.com',
            'phone' => '0111111114',
            'address' => 'Phnom Penh',
            'card_no' => '345679123',
            'type' => 'user',
            'remember_token' => '',
            'password' => bcrypt('password'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

        DB::table('tbl_users')->insert([
            'firstname' => 'aaron',
            'lastname' => 'maru',
            'username' => 'aaron',
            'email' => 'sai.vichet70@gmail.com',
            'phone' => '070707070',
            'address' => 'Phnom Penh',
            'card_no' => '345679123',
            'type' => 'manager',
            'remember_token' => '',
            'password' => bcrypt('password'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);

    }
}
