<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PlaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
        24 provinces with general post code
        reference link:
            http://www.stat.go.jp/info/meetings/cambodia/pdf/00dis_11.pdf
*/
        DB::table('tbl_places')->insert([
            'name' => 'Phnom Penh Thmei',
            // 'place_from' => 'Phnom Penh Thmei',
            // 'place_to' => 'Kampot',
            'code' => '12101',
            // 'description' => 'The capital city of Cambodia',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'BATTAMBANG',
            // 'place_from' => 'BATTAMBANG',
            // 'place_to' => 'KampongThom',
            'code' => '02000',
            // 'description' => 'battambang Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'KAMPONG CHAM',
            // 'place_from' => 'KAMPONG CHAM',
            // 'place_to' => 'Kep',
            'code' => '03000',
            // 'description' => 'Kampong cham Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'Kampong Chhnang',
            // 'place_from' => 'Kampong Chhnang',
            // 'place_to' => 'Siem Reap',
            'code' => '04000',
            // 'description' => 'Kampong Chhnang Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'KOMPONG SPUE',
            // 'place_from' => 'KOMPONG SPUE',
            // 'place_to' => 'Kampot',
            'code' => '05000',
            // 'description' => 'Kampong Speu Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'KAMPONG THOM',
            // 'place_from' => 'KAMPONG THOM',
            // 'place_to' => 'Kampot',
            'code' => '06000',
            // 'description' => 'Kampong Thom Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'KAMPOT',
            // 'place_from' => 'KAMPOT',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '07000',
            // 'description' => 'Kampot Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'KANDAL',
            // 'place_from' => 'KANDAL',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '08000',
            // 'description' => 'Kandal Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'KOH KONG',
            // 'place_from' => 'KOH KONG',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '09000',
            // 'description' => 'Koh Kong Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'KRATIE',
            // 'place_from' => 'KRATIE',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '10000',
            // 'description' => 'Kratie Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'MONDUL KIRI',
            // 'place_from' => 'MONDUL KIRI',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '11000',
            // 'description' => 'Mondul Kiri Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'PHNOM PENH',
            // 'place_from' => 'PHNOM PENH',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '12000',
            // 'description' => 'Phnom Penh the capital of city',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'PREAS VIHEA',
            // 'place_from' => 'PREAS VIHEA',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '13000',
            // 'description' => 'Preah Vihear Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'PREY VENG',
            // 'place_from' => 'PREY VENG',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '14000',
            // 'description' => 'Prey Veng Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'POVSAT',
            // 'place_from' => 'POVSAT',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '15000',
            // 'description' => 'Pursat Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'RATANAKIRI',
            // 'place_from' => 'RATANAKIRI',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '16000',
            // 'description' => 'Ratanak Kiri Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'SIEM REAP',
            // 'place_from' => 'SIEM REAP',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '17000',
            // 'description' => 'Siem Reap Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'PREAH SIHANOUK',
            // 'place_from' => 'PREAH SIHANOUK',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '18000',
            // 'description' => 'Preah Sihanouk Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'STUNG TRENG',
            // 'place_from' => 'STUNG TRENG',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '19000',
            // 'description' => 'Stung Treng Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_places')->insert([
            'name' => 'SVAY RIENG',
            // 'place_from' => 'SVAY RIENG',
            // 'place_to' => 'PHNOM PENH THMEY',
            'code' => '20000',
            // 'description' => 'Svay Rieng Province',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);



    }
}
