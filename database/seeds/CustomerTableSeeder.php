<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CustomerTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('tbl_customers')->insert([
            'firstname' => 'Ravuthz',
            'lastname' => 'Yo',
            'username' => 'yoravuthz',
            'email' => 'ravuthz@gmail.com',
            'phone' => '0964577770',
            'password' => bcrypt('passcode'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('tbl_customers')->insert([
            'firstname' => 'Ravuthz1',
            'lastname' => 'Yo1',
            'username' => 'yo1ravuthz1',
            'email' => 'ravuthz1@gmail.com',
            'phone' => '0964577771',
            'password' => bcrypt('passcode'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
