<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblPlaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            // $table->string('place_from',255);
            // $table->string('place_to',255);
            $table->string('code',5);
            /*$table->text('description');*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_places');
    }
}
