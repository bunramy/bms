<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblBooking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_booking', function (Blueprint $table) {
            /*$table->integer('place_from')->unsigned();
            $table->integer('place_to')->unsigned();
            $table->integer('time_id')->unsigned();
            $table->integer('time_start')->unsigned();
            $table->integer('time_stop')->unsigned();*/

            $table->increments('id');
            $table->boolean('type_trip')->unsigned();
            $table->integer('seats_amount')->unsigned();
            $table->integer('branch_id')->unsigned();

//            $table->integer('seats_price')->unsigned();

            $table->integer('user_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->timestamps();

            /*$table->foreign('place_from')->references('id')->on('tbl_places')->onDelete('cascade');
            $table->foreign('place_to')->references('id')->on('tbl_places')->onDelete('cascade');
            $table->foreign('time_id')->references('id')->on('tbl_times')->onDelete('cascade');
            $table->foreign('time_start')->references('id')->on('tbl_departures')->onDelete('cascade');
            $table->foreign('time_stop')->references('id')->on('tbl_departures')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('tbl_users')->onDelete('cascade');
            $table->foreign('customer_id')->references('id')->on('tbl_customers')->onDelete('cascade');
            $table->foreign('branch_id')->references('id')->on('tbl_branches')->onDelete('cascade');*/

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tbl_booking');
    }
}
