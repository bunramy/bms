<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblBuses extends Migration
{

    public function up()
    {
        Schema::create('tbl_buses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 8);
            $table->integer('driver_id')->unsigned();
            $table->text('park');
            $table->integer('type')->unsigned();
            $table->timestamps();

            $table->foreign('driver_id')->references('id')->on('tbl_drivers')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('tbl_buses');
    }
}
