<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TblDepartures extends Migration {

    public function up() {
        Schema::create('tbl_departures', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('time_start')->unsigned();
            $table->integer('time_stop')->unsigned();
            $table->timestamps();

            // $table->foreign('time_start')->references('id')->on('tbl_times')->onDelete('cascade');
            // $table->foreign('time_stop')->references('id')->on('tbl_times')->onDelete('cascade');
        });
    }

    public function down() {
        Schema::drop('tbl_departures');
    }

}
