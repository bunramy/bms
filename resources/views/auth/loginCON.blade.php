@extends('dash')


@section('content')


	{!! Form::open(['url' => '/auth/login', 'method' => 'put']) !!}
	{!! Form::open(array('url' => '/auth/register', 'class' => 'form-horizontal')) !!}
		<h2>Login For User</h2>
	    <div class="form-group">
	        {!! Form::label('email','Email')!!}
	        {!!Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'example@example.com'])!!}
	    </div>

	    <div class="form-group">
	        {!! Form::label('password','Password')!!}
	        {!!Form::password('password', ['class' => 'form-control'])!!}
	    </div>


		<div class="checkbox">
		    <label>
		      	 {!!Form::checkbox('remember', null); !!} Remember Me
		    </label>
		  </div>
	    <div class="form-group">
	        {!! Form::submit('Login', ['class' => 'btn btn-primary form-control'])!!}

	    </div>

	{!! Form::close() !!}
@stop