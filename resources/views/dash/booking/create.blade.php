@extends('dash')

@section('content')
    {!! Form::open(['route'=>'dash.booking.store', 'id'=>'frmBooking']) !!}
        @include('dash.booking.form',['submitButtonText'=>'Booking!','pageHeader'=>'Book the ticket!'])
    {!! Form::close() !!}
@stop

@include ('footer')

@push('scripts')

<script type="text/javascript">
    $(function(){
        
        console.log(places);


        $('#selFrom, #selTo').change(function(){
            var from = $('#selFrom').val();
            var to = $('#selTo').val();

            if (from == to) {
                alert("We don't have this destination!!!");
            } else {



            }
        });

    });
</script>

@endpush

